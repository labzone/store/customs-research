<?php

class CustomsResearch
{
    private static $instance;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    public function getContent($document)
    {
        $filename = __DIR__."/doc/{$document}.md";
        if (!file_exists($filename)) {
            throw new \Exception('File not found', 1);
        }
        $markdown = file_get_contents($filename);

        return \Parsedown::instance()->text($markdown);
    }

    public static function instance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }
}
