#Custom Procedure 

The Registration on Unit at TFCC will query from the Customs Management (CMS) Database and ensure that the applicant is already registered as importer/exporter. The application on form will then be forwarded to the Assessment Section on for compliance checks. If the applicant is not registered as importer/exporter at MRA, he/she will be requested to submit a duly filled in application on form (MRA/CUS/TFCC/REG/EO + EO01) for registration on as such together with copies of following documents:


###Document requirement
- Invoice - *indicating the FOB and/or CIF value of goods*
- Packing list - *A packing list is prepared by the exporter and it commonly includes all the details of the package contents, number of packages, carton numbers, net weight and gross weight and may or may not include customer pricing*
- Bill of landing
- Bill of entry
- Certificate of origin - *if applicable* - The Certificate of Origin indicates the country in which the goods are manufactured.  There are two types of certificates of origin: Non-preferential Certificate of Origin attests the origin of the products anddo not confer any tariff preferences. Preferential Certificate of Origin attests that the goods originate from a country with which a trade agreement has been signed and the goods are eligible for tariff preferences.

The registration  form is available on the MRA website on the following internet link:
[Customer Fill in form for Custom Clearance]
(http://www.mra.mu/download/EO01importerexporter1.pdf)



###Contact details for further Information:###
- Custom House, Mer Rouge, Port-Louis
- Tel: 2020500
- Email: registration.customs@mra.mu 
- or visit: Mauritius Revenue Authority's website at www.mra.mu  


###Useful Reference for more info on Mauritius Custom###
- http://investmentbusinessguide.mu/export_import_mauritius.html 
- http://mauritius.shipping-international.com/customs/
