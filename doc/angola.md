# Custom Procedure

## Document Requirement
- Bill of lading
- Commercial Invoice
- Parking List
- Certificate Of Origin
- Customs import declaration *Documento Único*

Documents to be sent by the exporter in the country of supply that are necessary for IFR issuance:
- Final invoice
- Packing list
- Bill of lading

## Fees

Fees associated with importing include:
- The Documento Unico costs US$10 per form
- Inspection fee 0.75% of assessed FOB value
- Clearing costs 2%
- VAT 2% to 30% depending on the good
- Revenue stamps 0.5%

## Regulations

- If containers have not been taken out of the terminal within 10 days from date of discharge the terminal operator is allowed to move them to an inland depot (second line terminal), all additional costs will be charged to the importer by the terminal operator.
- If no customs process has been started within 60 days from date of discharge the Angolan customs authorities are allowed to sell cargo by auction. Prior to selling any cargo by auction the customs authorities must announce in the newspaper the container numbers and the name of the manifested importers.
[Download and fill the Documento Unico for Custom Clearance](http://ec.europa.eu/agriculture/markets/export_refunds/forms/ao.pdf).

## Office Locations & Contacts

- Angola Customs contact information
- Address: Building MOF, Mutamba Largo da, Luanda, Angola
- Telephone: +(244)222338548
- Website: http://www.minfin.gv.ao/

See the link below for more information on Angola Shipping procedures
- http://export.gov/logistics/eg_main_018121.asp
- https://classic.maerskline.com/link/?page=lhp&path=/africa/angola/import/procedures
- http://www.economy.gov.tr/portal/content/conn/UCM/uuid/dDocName:EK-
- http://www.economy.gov.tr/portal/content/conn/UCM/uuid/dDocName:EK-214332;jsessionid=WzOB6ZY_3f9BoEL94cC1UxxYbqQkdzdGXZmOTfrCF0oDtlFrFzXS!-475025689
- http://www.wikiprocedure.coms/index.php/Angola_-_Importing_goods
