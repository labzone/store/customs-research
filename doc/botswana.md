# Custom Procedure

### Document Requirement
- Commercial invoice
- Bill of lading
- Certificate of origin
- Parking list
- Identification card

When goods are imported into Botswana, they must immediately be declared to BURS (Customs & Excise Division) against an appropriate custom form with supporting documents (import license, invoice, certificates of origin etc.) Customs procedures have been computerized using UNCTAD's ASYCUDA, enabling traders to submit customs declarations electronically (Direct Trader Input) using the single administrative document (SAD). 
[Download the form here](http://www.burs.org.bw/phocadownload/General_Downloads/SAD%20500_Customs_Declaration_Form.pdf)

### Custom Contact Information for Any Query
- Botswana Unified Revenue Service
- website: http://www.burs.org.bw 
- Commissioner Customs & Excise: Mr. K. Morris Tel: +267 363 9503 Fax: +267 3163955. Fax: +267 395 1918.
- Email: kmorris@burs.org.bw 

### Notes: 
Customer must bring all their document suggested above for custom Process 
Click on the following click for more information on custom In Botswana:
- http://www.burs.org.bw/index.php/customsexcisemain/customs-clearance 
- http://www.burs.org.bw/phocadownload/General_Downloads/SAD%20500_Customs_Declration_Form.pdf 

