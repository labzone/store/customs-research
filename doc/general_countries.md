
# Import and Export Procedures

#### Shipping Countries 
- Angola
- Botswana
- Burundi
- Comoros
- Equatorial Guinea
- Gabon
- Kenya
- Lesotho
- Madagascar
- Malawi
- Mauritius
- Mayotte
- Mozambique
- Namibia
- Republic of Congo
- Republic Democratic of the Congo
- Reunion
- Rwanda
- Seychelles
- South Africa
- Swaziland
- Tanzanie
- Uganda
- Zambia
- Zimbabwe

#### What is the procedure and the requirements for importation into the country and clearance through Customs?

When a shipment arrives, importers are required to file a Goods Declaration and supporting documents for the imports with a Customs officer at the port of entry. Imported cargo are not legally entered ,until after the shipment has arrived within the port of entry, delivery of the merchandise has been authorized by Customs, and applicable taxes and duties have been paid.
Useful link: 	
[Link 1](http://exporthelp.europa.eu/thdapp/display.htm?page=rt/rt_DocumentsForCustomsClearance.html&docType=main&languageId=EN)
[Link 2](http://www.matrade.gov.my/en/malaysia-exporters-section/119-going-global-beginners-guide/636-logistics-and-export-documentation)




#### What is the basic documentation that is required for imports and exports clearance?
A commercial invoice is used as a supporting document for most trade procedures. Your freight forwarder or clearing agent should be able to arrange for most of the other import or export procedures that may be applicable by using the information contained in the invoice. The information should include all description of the goods, value, gross and net weight and country of origin of items. The Bill of Lading provides evidence of the contract between the exporter and carrier (ship). It evidences receipt of the goods into the custody of the carrier. If the goods are transported by air, then an airway bill is used. Another basic documentation is the packing list, which shows details such as marks and number of packages, gross and net weights, measurements and description of contents of each package. In addition, necessary certificates and permits need to accompany the documents. These include documents such as Certificates of Origin, Phytosanitary Certificates, import permits, etc.
http://www.mauritiustrade.mu/en/faq/ 
 Ministry of Industry, Commerce & Consumer Protection (Guidelines - Published in March 2012)
What supporting documentation do I require? (Mauritius)
This is dependent on the type of product being shipped. Basic documentation would be :
Document Requirement 
- Import Permit
- Packing list
- Preference Certificates
- Certificate of Origin
- Original Invoice
- Shipping document ( Bill of Lading )
- Supplier's commercial invoice
[Please found more information about Custom Clearance can be found here](http://www.sotrimex.mu/customs_clear.htm) 
Preparing For Your Shipment
So you’ve ordered your products and they are finally ready to be shipped to you. What now? There are two main ways to get your goods into your home country, by air and by sea.
But before you initiate your shipments, make sure you ask for the following documents from your vendor. These will be required in order to clear customs.(Vendor Need to provide)
 Invoice – The declared value of the items
A Packing List – What you are importing
A Detail Sheet – Outlines how the products are manufactured so customs can determine the duty classification
Bill of Lading- document issue by carrier which details shipment of goods and give titles of that shipment to a specified party.
http://mywifequitherjob.com/how-to-import-goods-from-overseas-for-your-online-store/ 

Customs Formalities & Procedures For Clearance of Goods
 The importation of any goods requires the importer to complete Customs Formalities. These formalities include the submission of a Customs Declaration, commonly known as the Bill of Entry (BOE), electronically through the TradeNet giving all details of the goods imported, such as the quantity, value and precise nature of the goods. In addition to the BOE, trade documents such as invoices, bills of lading, permits or other documents accompanying the import should also be scanned and submitted electronically. Once submitted the customs declaration is validated by customs and a message is sent back to the declarant and payment of customs charges, if any, is effected. The BOE is then processed at the Compliance Section whereby after checking the documents, the compliance officer may: 
release the goods;
require clarifications about documents submitted; or
send the declaration for verification.

[For more information click here](http://www.orbitmoving.com/moving-to-Mauritius) 



Customs
All importers must be registered with the Customs Department. On arrival of a consignment, a company must complete Customs formalities which consist in submitting a Customs Declaration (also called Bill of Entry) electronically through the Tradenet System (http://mns.mu/tradenet-trade-facilitation.php) giving all details of the goods imported such as the quantity, value and precise nature of the goods.
An importer may use the services of a Freight Forwarding/Clearing Agent (A list of Freight Forwarding Agents can be obtained at: www.aptmauritius.org) or a Customs Broker (A list of Customs House Brokers can be obtained at: www.customshousebro- kers.com) for the Customs formalities and the clearing of goods. Any person who is a registered user of the TradeNet system 
may also submit the Customs declaration.








#### Main Documents for Import
The following documents are required at importation:
Invoice indicating the FOB and/or CIF value of goods
Packing list (A packing list is prepared by the exporter and it commonly includes all the details of the package contents, number of packages, carton numbers, net weight and gross weight and may or may not include customer pricing)
Bill of landing / Airway bill
Bill of entry
Insurance certificate (if applicable) - A certificate of inspection is required by some importers and/or importing countries.      It is sometimes also refered to as the “Pre-shipment Inspection” (PSI) certificate and is issued by an independent third party confirming the buyer’s specifications, quantity and value of goods prior to shipment.
Certificate of inspection (if applicable) -
Certificate of origin (if applicable) - The Certificate of Origin indicates the country in which the goods are manufactured.  There are two types of certificates of origin: Non-preferential Certificate of Origin attests the origin of the products and        do not confer any tariff preferences. Preferential Certificate of Origin attests that the goods originate from a country with which a trade agreement has been signed and the goods are eligible for tariff preferences.
The other documents (required by government agencies (e.g. health, agriculture and veterinary services)










#### Angola shipping procedures 
Procedure
The exporter issues a Proforma Invoice, which must be sent to the consignee in Angola
The consignee must then send the document to an official Angolan customs agent/ broker so that a provisional DU may be presented to the Ministry (in paper and magnetic format) and file the exemption
The broker declares the goods using the Documento Único (single document).
When goods arrive at the port, they go through Pre-Shipment Inspection (if the goods exceed some stated value).
After completing the pre-shipment inspection, the importer will be issued with a verification certificate, the Clean Report of Findings (CRF)
The original of the CRF will be delivered to you (the importer) by the inspecting company in Luanda, in order for the goods to clear customs. If the final documents are missing or if there are differences between the application and the goods a Non Negotiable Report of Findings (NNRF) will be issued.
For goods not subject to PSI, the importer must complete a Declaration of Customs Value (ADV), providing all necessary information to obtain the customs value
The importer will then either be issued with an inspection note from the Regional Director or be issued with a payment note.
Upon payment of customs duties, the importer may clear the goods.
After payment of duties, the Documento de Arrecadadçao de Receitas is issued as proof of duty payment and the Nota de Desalfandegamento is issued to prove customs clearance
http://www.wikiprocedure.com/index.php/Angola_-_Importing_goods 





Important:
Bill of lading • Cargo release order • Certificate of origin • Commercial Invoice • Foreign exchange authorization • Inspection report • Packing list pack • Tax certificate • Technical standard/health certificate • Terminal handling receipts
http://sourcemauritius.com/wp-content/uploads/2014/03/Doing-Business-in-Angola.pdf 
Custom Clearance of Botswana
https://www.iamovers.org/files/newimages/member/shippers/botswana.pdf
	Documents Required  Original Customs Form CE 101 (2 copies)∙  Copy of Passport, including page entry date into Botswana∙  Long-Term Permit or Permanent Work / Residence Permit∙  Letter of Employment (stating that duration of contract is greater than 2 years)∙  Commercial invoices – see Specific Regulations (3 copies)∙  Detailed inventory with declared value∙  Diplomatic Clearance (Diplomats)∙  Certificate "A" from relevant Embassy in Botswana, signed by Botswana Department of∙ Foreign Affairs (Diplomats)	
Document needed generally:

What documents need to accompany my declaration? When you use DTI and submit your declaration electronically, you must retain accompanying documents for customs inspection/audit for a period of three years from the end of the year in which the goods are released from Revenue control. Examples of supporting information required are: - the invoice on which the Customs value of the goods is declared; - a value declaration on Form C&E No. G563; - documents required for preferential trade agreements or other reliefs from duty (e.g. origin documents and bills of lading); and - other documents required under provisions governing the release for free circulation of the goods e.g. import licences

General Export Procedures
Export procedure start with the exporter booking for space in a vessel or aircraft to carry the cargo. Some shipping line requires booking to be made 4-6 weeks before the vessel arrives at the port of Mombasa.  The exporter will also book for a container if the goods are to go by sea way in advance.
 
Once the goods are ready to be packed into the container the exporter will have prepared, the commercial invoice the packing list, permits and others mentioned above.  The exporter will book for a customs officer to come to the packing warehouse to verify the export goods and take photographs of the goods.  Once the container is packed the proper officer will then put a customs seal. The exporter through the clearing agent will then proceed to lodge the documents into the Simba system for customs to process and release the goods for export.
 
The clearing agent will then deliver the goods to the shipping line and once the goods are accepted by the master of the vessel, a bill of lading will be issued by the shipping agent and the exporter will collect the bill of lading.  The exporter   will then take all the export documents and send them to the bank if payment for the goods is by Letter of Credit. If the payment is by other methods, he/she will send the documents by courier services. Port handling charges as well as transport costs will be met by the exporter depending on INCO-TERMS agreed on between importer and exporter. 
See the link below for more information on export procedure for different countries:
http://www.shipperscouncilea.org/index.php?option=com_vervtrack&view=taggeditems&layout=details&tmpl=component&print=1&hscode=0901&Itemid=38



The process of Custom Clearance 
The Import process can be summed up as per following:
To initiate the Import process in Dubai, the importer must obtain following documents in original from the exporter: 

Original Bill of Lading copies
Commercial Invoice
Packing List
Certificate of Origin
The above documents are required irrespective of the sales agreement and payment method agreed between the importer and exporter.

The importer can then login to Dubai Trade to create and submit the Import Declaration application for Dubai Customs clearance. The importer can make payment for customs duties and other fees online using the CDR account or E-payment using credit card or direct debit. In case of restricted goods or duty exempted cargo, the importer must arrange for necessary permits from the designated Permit Issuing Authority before filling the Customs Import Declaration. For import of goods from outside the country into Dubai following documents are required:

Commercial invoice from the exporter addressed to the importer detailing total quantity, goods description and total value for each item
Original certificate of origin approved by the chamber of commerce at the country of origin detailing the origin of goods.
Detailed packing list as per weight, method of packing and HS code for each individual article contained in the shipment.
Import permit from the competent agencies in the event of importing restricted goods or duty exempted goods.

[More information on click here](http://www.dubaitrade.ae/knowledge-centre/processes-a-procedures/216-diagram-of-import-process-for-dtp)

ASYCUDA Automated System for Customs Data for Customs Data
Customs procedures have been computerized using UNCTAD's ASYCUDA, enabling traders to submit customs declarations electronically (Direct Trader Input) using the single administrative document (SAD). 
Using ASYCUDA SYSTEM
- Botswana 
- Burundi
- comoros 
- congo
- Congo DR
- Gabon
- Gambia
- Guinea
- Madagascar
- malawi
- Namibia
- Nigeria
- Rwanda
- Tanzania
- Zambia
- Zimbabwe

[More information on Asycuda](http://unctad.org/en/Docs/TN21_Asycuda.pdf)


#### Southern African Development Community (SADC)


Duty Free Access to SADC Member States: Exports between SADC FTA Member States is duty free for all products except for Malawi, Zimbabwe and Tanzania. These countries still maintain customs duties on certain products. 

- Angola
- Botswana
- Democratic Republic of Congo
- Lesotho
- Madagascar
- Malawi
- Mauritius
- Mozambique
- Namibia 
- Seychelle
- South Africa
- Swaziland
- United Republic of Tanzania
- Zambia
- Zimbabwe.
	
