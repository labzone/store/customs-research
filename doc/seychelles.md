#Custom Procedure 

All goods imported into Seychelles have to pass through Customs procedures and the correct declarations have to be made to Customs. When making a declaration, the person must accept the responsibilities under the law for the accuracy of the given information, authenticity of the documents being provided and the observance of all the obligations necessary under the declared procedures.
###Document requirement
- Original Invoice
- Packing List
- Bill of Lading
- Identification card

Any good brought into seychelles must be covered by a summary declaration. It can be in written statement providing a custom with advance information necessary for risk profiling. A summary declaration can be in hard copy or/ electronic format. Declaration must be submitted to customs office within 24 hours, excluding weekends or public holidays. Customer must insure that correct model of the declaration is selected to ensure that the correct type of control and data of the ASYCUDA World system is available. Bills of Entry are required for the following models of declaration.
- [Models of Declaration can be found here](http://www.src.gov.sc/pages/customs/importandexport.aspx)


You will need to get an agent to make your Bill of entry.
First you will need to provide your clearing agent  all necessary document to make you Bill of entry.
You agent will send the bill of entry  thro asycuda system online to customs to process.  
You will then be inform by your agent when it is ready to make payment  to customs. 
Registered clearing agent have certain formalities that they need to do to get access with the system.


The following taxes are collected at the point of entry on importation by custom division on behalf of seychelles revenue commision:

- Trade Tax 
- VAT 
- Excise Tax 

####For more information on Custom procedure click the link below:

- http://www.src.gov.sc/pages/customs/importandexport.aspx#Documentation 

- http://www.src.gov.sc/resources/Guides/CustomsClearanceProcedure.pdf 

- http://www.nation.sc/article.html?id=234731 


####For more information contact:
- Seychelles Revenue Commission 
- Phone: 4293737
- Email: commissioner@src.gov.sc 


