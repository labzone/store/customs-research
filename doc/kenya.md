# Custom Procedure 

### Document Requirement 

- Bill of lading 
- Packing list 
- Commercial Invoice 
- Identification card ID
- Import declaration form IDF
- Certificate of conformity CoC


### Customs Declaration 
An Import Declaration Form (IDF) is required for all shipments with an invoice value above US$1,000 and/or any with total weight exceeding 70 Kgs and/or those whose description include Spare parts. An IDF may be obtained by the consignee from the Kenya Revenue Authority (KRA) before arrival of shipment.
If a shipment that requires official application and an IDF arrives in Kenya and the consignee has not obtained an IDF from KRA in advance, it can be done by UPS on behalf of the importer at a cost of KES 2,320 application fee + IDF(GOK) fee - KES 5,000 or 2.25% of CIF value, whichever is higher.


The documents will enable Electronic Registration of The Customs Entry. The registered entry will be passed for eventual release of the goods upon: 
- Payment of Import Taxes 

[Download Import declaration form (IDF) for custom](http://www.kra.go.ke/customs/pdf/Import_Declaration_Fee_%20FORM.pdf)

More information on Custom Clearance:
- http://www.kra.go.ke/customs/pdf/Import_Declaration_Fee_%20FORM.pdf 


### Contact information:
- KRA Call Centre
- Tel:  +254 (020) 4999 999
- Cell: +254 (0711) 099 999
- Email: callcentre@kra.go.ke




