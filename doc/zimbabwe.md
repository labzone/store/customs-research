#Custom Procedure 

### Document Requirement

- Bill of entry (issue by clearing agent at the port)
- Supplier invoice (provided by the seller through email)
- Bill of lading (will be provided shipper when your goods reach seychelles) 
- Certificate of origin (will be provide by the seller if required)
- Packing list (Obtain from the seller)
- Identification number (ID)


1. Customers have to provide their document to their clearing agent as soon as your goods arrived in the country.
1. Documentation has to be done by either a registered importer or a registered clearing agent. No duties are payable on export but clearance fees will be due and payable.
1. All customers have to cleared their goods by using a clearing agent to capture their import information on a bill of entry.
1. The agent will issue a bill of entry using relevant document.
1. Bill of entry will be process through the asycuda system online for custom to process.
1. Then you will be inform by your agent when it is ready to make payment to customs.

[Download custom declaration forms and fill for clearance](http://www.zimra.co.zw/index.php?option=com_phocadownload&view=category&id=6:customs&Itemid=112) 

###For further information, please contact your nearest ZIMRA office.

- Zimbabwe Revenue Authority
- Legal and Corporate Services Division
- 6th Floor, ZB Centre
- Cnr First Street/Kwame Nkrumah Avenue
- P O Box 4360
- Harare
- Tel: 04 – 775332/751624/781345
- Fax: 04 – 774087
- E-Mail: pr@zimra.co.zw
- Website:www.zimra.co.zw.

[See the link below for more information on Zimbabwe Shipping procedures](http://www.zimra.co.zw/index.php?option=com_content&view=article&id=59&Itemid=54) 
 
[Information on custom duty and taxes](http://www.zimra.co.zw/index.php?option=com_content&view=article&id=1549:duty-on-private-imports&catid=21:did-you-know&Itemid=91)
