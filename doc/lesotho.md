# Custom Procedure

### Document Requirement :

- Commercial Invoice
- Transport document *Bill of lading*
- ID *Identification card*
- Completed SAD form Single Administrative document


Upon entry into Lesotho, an importer is required to make a written declaration of goods and services regardless of their value. All goods are subject to examination by the LRA Customs. The declaration is made using official a declaration form that is used and accepted in whole of the Southern African Customs Union *SACU* 1 region. This form is known as a Single Administrative Document, commonly referred to as “SAD form”. All goods entering and leaving Lesotho should be declared fully, unreservedly and all documentation relating there should also be submitted with the SAD form as per section 14 of Customs and Excise Act No. 10 of 1982. 


[Downloads SAD form here for custom procedure](http://www.lra.org.ls/Downloadable_Docs/taxes/Customs/Downloads/Forms/SAD%20500%20Customs%20Declarations%20Form%20060920.pdf)







More information on custom:
http://www.lra.org.ls/Downloadable_Docs/taxes/Customs/Downloads/Brochures/Guidelines%20for%20Extra%20SACU%20importation%20and%20exportation2010.pdf 



