# Custom Procedure

### Document Requirement
- Commercial Invoice 
- Bill of lading
- Packing list
- Certificate of origin
- Proof of importer’s identify (passport, national identity card or driving license)

### Procedures 
- 1.Traders who wish to import goods to Burundi should start by finding out the requirements of becoming a successful importer in the country. This is done by visiting and making consultation at the Burundi customs office the Burundi Revenue Authority (Office Burundais des Recettes, OBR);
- 2.When the above information is obtained, the applicant should proceed with preparing the required documents and where necessary obtain licenses, permits and certificates. The documents required caries basing on the type of goods to be imported as some goods require special permits whereas so do not;
- 3.When good have arrived, and the applicant / importer is done with preparation of document, he / she then proceeds with customs clearance and technical control. Upon arrival, the goods are declared to the authority at the port or place of arrival.
This is done by either the master of the vessel or master of an aircraft (depending on the means of transport used) who obtains an appropriate form, fills it accurately and submits it together with required documents to the authority;
- 4.When goods are declared, port and terminal handling proceeds. Here goods are handled to authorities at the entry point for clearing. In this case the goods in the vessel are cross check and verified against the submitted documents;
- 5.Thereafter there is inland transportation and handling. The goods are transported to a warehouse / store as they await for the import to clear customs duties;
- 6.When duties are cleared, the importer can then take the goods to his store;

[Download Custom declaration form C17 for custom clearance](http://www.customs.eac.int/index.php?option=com_content&view=article&id=88:c17-a&catid=34:forms)

### Office Locations & Contacts
- The Burundi Revenue Authority (Office Burundais des Recettes, OBR)
- B.P 3465 Bujumbura 11 Burundi
- Telephone: 22276142
- Email: info@obr.gov.bi
- Website: http://english.obr.bi/home.html

Follow the link below for more information on importation in Burundi:
- http://www.wikiprocedure.com/index.php/Burundi_-_Import_goods#Documents_to_Use 
- http://www.shipperscouncilea.org/index.php?option=com_vervtrack&view=taggeditems&layout=details&tmpl=component&print=1&hscode=0901&Itemid=38

